# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161106122039) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "balances", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "month",                      null: false
    t.integer  "year",                       null: false
    t.decimal  "amount",     default: "0.0", null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["user_id"], name: "index_balances_on_user_id", using: :btree
  end

  create_table "categories", force: :cascade do |t|
    t.string   "title",      limit: 50, null: false
    t.string   "type",       limit: 50
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "user_id"
    t.index ["title"], name: "index_categories_on_title", using: :btree
    t.index ["user_id"], name: "index_categories_on_user_id", using: :btree
  end

  create_table "families", force: :cascade do |t|
    t.string   "title",      limit: 50,                 null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "paid",                  default: false
  end

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["provider"], name: "index_identities_on_provider", using: :btree
    t.index ["uid"], name: "index_identities_on_uid", using: :btree
    t.index ["user_id"], name: "index_identities_on_user_id", using: :btree
  end

  create_table "invites", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "code",        limit: 32, null: false
    t.string   "email",       limit: 50, null: false
    t.datetime "sent_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.datetime "accepted_at"
    t.index ["user_id"], name: "index_invites_on_user_id", using: :btree
  end

  create_table "operation_categories", force: :cascade do |t|
    t.integer  "operation_id"
    t.integer  "category_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["category_id"], name: "index_operation_categories_on_category_id", using: :btree
    t.index ["operation_id"], name: "index_operation_categories_on_operation_id", using: :btree
  end

  create_table "operations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "type"
    t.decimal  "amount",                null: false
    t.date     "date",                  null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "title",      limit: 50
    t.index ["user_id"], name: "index_operations_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "family_id"
    t.index ["family_id"], name: "index_users_on_family_id", using: :btree
  end

  add_foreign_key "balances", "users"
  add_foreign_key "categories", "users"
  add_foreign_key "identities", "users"
  add_foreign_key "invites", "users"
  add_foreign_key "operation_categories", "categories"
  add_foreign_key "operation_categories", "operations"
  add_foreign_key "operations", "users"
  add_foreign_key "users", "families"
end
