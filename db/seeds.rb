# This file should contain all the record creation needed to seed the
# database with its default values.
# The data can then be loaded with the rails db:seed command
# (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create(
#     [{ name: 'Star Wars' }, { name: 'Lord of the Rings' }]
#   )
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(name: 'pustserg', email: 'pustserg@money.os')

20.times do |i|
  rand_amount = Faker::Commerce.price
  rand_date = Faker::Date.backward(30)
  Operations::Income.create(
    title: "income - #{i}",
    user: user,
    amount: rand_amount,
    date: rand_date
  )
  rand_amount = Faker::Commerce.price
  rand_date = Faker::Date.backward(30)
  Operations::Outgo.create(
    title: "outgo - #{i}",
    user: user,
    amount: rand_amount,
    date: rand_date
  )
end
