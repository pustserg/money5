class ChangeCategoriesUserIdToReferences < ActiveRecord::Migration[5.0]
  def change
    remove_columns :categories, :user_id
    add_reference :categories, :user, index: true, foreign_key: true
  end
end
