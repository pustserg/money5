class AddPaidToFamilies < ActiveRecord::Migration[5.0]
  def change
    add_column :families, :paid, :boolean, default: false
  end
end
