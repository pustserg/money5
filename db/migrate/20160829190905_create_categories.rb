class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :title, limit: 50, index: true, null: false
      t.string :user_id, index: true, null: false
      t.string :type, limit: 50

      t.timestamps
    end
  end
end
