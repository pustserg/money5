class AddAcceptedAtToInvites < ActiveRecord::Migration[5.0]
  def change
    add_column :invites, :accepted_at, :datetime
  end
end
