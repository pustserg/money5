class CreateInvites < ActiveRecord::Migration[5.0]
  def change
    create_table :invites do |t|
      t.references :user, foreign_key: true
      t.string :code, limit: 32, null: false
      t.string :email, limit: 50, null: false
      t.datetime :sent_at

      t.timestamps
    end
  end
end
