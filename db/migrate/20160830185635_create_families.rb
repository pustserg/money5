class CreateFamilies < ActiveRecord::Migration[5.0]
  def change
    create_table :families do |t|
      t.string :title, null: false, limit: 50

      t.timestamps
    end
  end
end
