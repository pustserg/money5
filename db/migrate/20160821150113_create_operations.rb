class CreateOperations < ActiveRecord::Migration[5.0]
  def change
    create_table :operations do |t|
      t.references :user, foreign_key: true, index: true
      t.string :type
      t.decimal :amount, null: false
      t.date :date, null: false

      t.timestamps
    end
  end
end
