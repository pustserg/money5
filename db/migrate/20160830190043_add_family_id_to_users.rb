class AddFamilyIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :family, foreign_key: true, index: true
  end
end
