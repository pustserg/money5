class AddTitleToOperations < ActiveRecord::Migration[5.0]
  def change
    add_column :operations, :title, :string, limit: 50
  end
end
