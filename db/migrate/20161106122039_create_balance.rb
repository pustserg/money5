class CreateBalance < ActiveRecord::Migration[5.0]
  def change
    create_table :balances do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :month, null: false
      t.integer :year, null: false
      t.decimal :amount, null: false, default: 0
      t.timestamps
    end
  end
end
