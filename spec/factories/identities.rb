FactoryGirl.define do
  factory :identity do
    user
    provider 'facebook'
    uid '9dsfjl9'
  end
end
