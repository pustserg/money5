FactoryGirl.define do
  factory :invite do
    email { Faker::Internet.email }
    code { Faker::Crypto.md5 }
    user
  end
end
