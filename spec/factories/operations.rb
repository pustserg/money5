FactoryGirl.define do
  factory :operation do
    user
    amount { Faker::Commerce.price }
    date { Faker::Date.between(Time.current.beginning_of_month, Date.current) }
    title { Faker::Lorem.word }
  end

  factory :income_operation, parent: :operation, class: 'Operations::Income' do
  end

  factory :outgo_operation, parent: :operation, class: 'Operations::Outgo' do
  end
end
