FactoryGirl.define do
  factory :operation_category do
    operation
    category
  end
end
