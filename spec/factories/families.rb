FactoryGirl.define do
  factory :family do
    title { Faker::Lorem.word }
  end
end
