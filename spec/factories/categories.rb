FactoryGirl.define do
  factory :category do
    title { Faker::Lorem.word }
    user
  end

  factory :income_category, parent: :category, class: 'Categories::Income' do
  end

  factory :outgo_category, parent: :category, class: 'Categories::Outgo' do
  end
end
