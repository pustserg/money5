FactoryGirl.define do
  factory :balance do
    user
    month { Date.current.month }
    year { Date.current.year }
  end
end
