require 'rails_helper'
require 'capybara/poltergeist'
Dir['./spec/support/*.rb'].sort.each { |f| require f }
RSpec.configure do |config|
  Capybara.javascript_driver = :poltergeist

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.strategy = :truncation
  end

  config.before(:each, js: true) do
    DatabaseCleaner.strategy = :truncation
  end
end
