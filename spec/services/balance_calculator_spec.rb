require 'rails_helper'

RSpec.describe BalanceCalculator do
  context 'current_balance' do
    let!(:user) { create(:user) }
    let!(:current_incomes) { create_list(:income_operation, 20, user: user) }
    let!(:current_outgos) { create_list(:outgo_operation, 20, user: user) }
    let!(:last_incomes) do
      create_list(
        :income_operation, 10, user: user, date: Date.current - 1.month
      )
    end
    let!(:last_outgos) do
      create_list(
        :outgo_operation, 10, user: user, date: Date.current - 1.month
      )
    end
    let!(:future_incomes) do
      create_list(
        :income_operation, 10, user: user, date: Date.current + 1.month
      )
    end
    let!(:future_outgos) do
      create_list(
        :outgo_operation, 10, user: user, date: Date.current + 1.month
      )
    end
    it 'should save current user balance' do
      expected_balance = current_incomes.sum(&:amount) -
                         current_outgos.sum(&:amount)
      described_class.new(user.id).call
      expect(user.current_month_balance_amount).to eq expected_balance
    end

    it 'should recalculate all balance if called with mode :all' do
      all_incomes = (current_incomes + last_incomes + future_incomes)
                    .sum(&:amount)
      all_outgos = (current_outgos + last_outgos + future_outgos)
                   .sum(&:amount)
      expected = all_incomes - all_outgos
      described_class.new(user.id).call(:all)
      expect(user.current_balance_amount).to eq expected
    end
  end
end
