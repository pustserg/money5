require 'rails_helper'

RSpec.describe CalculateUserBalanceJob, type: :job do
  let!(:user) { create(:user) }
  before do
    expect_any_instance_of(BalanceCalculator).to receive(:call).with(no_args)
  end

  it 'should call BalanceCalculator without args' do
    described_class.perform_now(user.id)
  end
end
