require 'rails_helper'

RSpec.describe OperationForm do
  context '#initialize' do
    it 'should be initialized with 0 amount unless amount given' do
      form = OperationForm.new
      expect(form.amount).to eq 0.0
    end

    it 'should be initialized with given amount if amount given' do
      rand_amount = Faker::Commerce.price
      form = OperationForm.new(amount: rand_amount)
      expect(form.amount).to eq rand_amount
    end

    it 'should be initialized with current date unless date given' do
      form = OperationForm.new
      expect(form.date).to eq Date.current
    end

    it 'should be initialized with given date if date given' do
      rand_date = Faker::Date.backward(10)
      form = OperationForm.new(date: rand_date)
      expect(form.date).to eq rand_date
    end
  end

  context '.persist!' do
    let!(:user) { create(:user) }
    before do
      ActiveJob::Base.queue_adapter = :test
    end
    it 'should recalculate user balance' do
      form = described_class.new({ type: 'income', amount: 100 }, user)
      form.persist!
      expect(CalculateUserBalanceJob).to have_been_enqueued
    end
  end

  context 'validations' do
    let!(:user) { create(:user) }
    before do
      create_list(:operation, Family::FREE_MONTH_OPERATIONS_LIMIT, user: user)
    end

    it 'should be valid if user has paid account' do
      allow(user).to receive(:paid?).and_return(true)
      form = described_class.new({ type: 'income', amount: 100 }, user)
      expect(form).to be_valid
    end
    it 'should not be valid if user has not paid account' do
      allow(user).to receive(:paid?).and_return(false)
      form = described_class.new({ type: 'income', amount: 100 }, user)
      expect(form).to_not be_valid
    end
  end
end
