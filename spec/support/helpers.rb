module OmniAuth
  module Mock
    def auth_mock
      OmniAuth.config.mock_auth[:facebook] = {
        provider: 'facebook',
        uid: '12345',
        info: {
          name: 'mock_user',
          email: 'mock_user@email.local'
        },
        credentials: { token: 'mock_token' }
      }.with_indifferent_access
    end
  end

  module SessionHelpers
    def signin
      visit root_path
      expect(page).to have_content('Login')
      auth_mock
      click_on 'Login'
    end
  end
end
RSpec.configure do |config|
  config.include OmniAuth::Mock
  config.include OmniAuth::SessionHelpers, type: :feature
end

OmniAuth.config.test_mode = true

def t(args)
  I18n.t(args)
end

def stub_current_user(user)
  allow_any_instance_of(ApplicationController).to(
    receive(:current_user).and_return(user)
  )
end
