require 'rails_helper'

RSpec.describe Operation, type: :model do
  it { should validate_presence_of(:amount) }
  it { should validate_presence_of(:date) }
  it { should validate_length_of(:title).is_at_most(50) }
  it { should belong_to :user }
  it { should have_many(:operation_categories) }
end
