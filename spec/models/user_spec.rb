require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:name) }
  it { should have_many :operations }
  it { should have_many :identities }
  it { should have_many :categories }
  it { should have_many :invites }
  it { should have_many :balances }
  it { should belong_to :family }
end
