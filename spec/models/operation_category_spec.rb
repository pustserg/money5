require 'rails_helper'

RSpec.describe OperationCategory, type: :model do
  it { should belong_to(:category) }
  it { should belong_to(:operation) }
end
