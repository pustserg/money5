require 'rails_helper'

RSpec.describe Category, type: :model do
  subject { create(:category) }
  it { should validate_presence_of(:title) }
  it { should validate_length_of(:title).is_at_most(50) }
  it { should validate_uniqueness_of(:title).scoped_to([:user_id, :type]) }
  it { should belong_to(:user) }
end
