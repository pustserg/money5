require 'rails_helper'

RSpec.describe Balance, type: :model do
  it { should validate_presence_of(:year) }
  it { should validate_presence_of(:month) }
  it { should validate_presence_of(:amount) }
  it { should belong_to(:user) }
end
