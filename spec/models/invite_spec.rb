require 'rails_helper'

RSpec.describe Invite, type: :model do
  it { should validate_presence_of(:user_id) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:code) }
end
