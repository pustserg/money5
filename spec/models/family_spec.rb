require 'rails_helper'

RSpec.describe Family, type: :model do
  subject { create(:family) }
  it { should have_many :users }
  it 'should not be paid initially' do
    expect(subject.paid?).to be_falsey
  end

  describe '.full?' do
    it 'should be full if users count equal to MAX_USER_COUNT' do
      allow(subject).to(receive_message_chain(:users, :count)) do
        Family::MAX_USERS_COUNT
      end
      expect(subject.full?).to be_truthy
    end
    it 'should be full if users count more than MAX_USER_COUNT' do
      allow(subject).to(receive_message_chain(:users, :count)) do
        Family::MAX_USERS_COUNT + 1
      end
      expect(subject.full?).to be_truthy
    end
    it 'should not be full if users count less than MAX_USER_COUNT' do
      allow(subject).to(receive_message_chain(:users, :count)) do
        Family::MAX_USERS_COUNT - 1
      end
      expect(subject.full?).to be_falsey
    end

    context 'invites are also takes into account' do
      let(:family) { create(:family) }
      let!(:user) { create(:user, family: family) }

      it 'should count users count with not accepted invites' do
        create_list(:invite, Family::MAX_USERS_COUNT - 1, user: user)
        expect(family.full?).to be_truthy
      end

      it 'should not count accepted invites' do
        invites = create_list(:invite, Family::MAX_USERS_COUNT - 1, user: user)
        invites.first.update!(accepted_at: Time.current)
        expect(family.full?).to be_falsey
      end
    end
  end
end
