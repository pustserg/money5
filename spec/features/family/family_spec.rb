require 'integration_helper'

feature 'User can edit family settings', js: true do
  let(:user) { create(:user) }
  before { stub_current_user(user) }

  scenario 'user edits family name' do
    new_name = Faker::Lorem.word
    visit incomes_path
    click_on user.name
    click_on t('common.settings')
    click_on t('common.edit')
    fill_in 'user_form_family_title', with: new_name
    click_on t('common.save')
    expect(user.reload.family_title).to eq new_name
  end

  scenario 'user can see other users from same family at profile' do
    other_user = create(:user, family: user.family)
    visit profile_path
    expect(page).to have_content other_user.name
  end

  context 'adding user to family' do
    scenario 'user can invite other users to email if family is not full' do
      email = Faker::Internet.email
      visit profile_path
      click_on t('family.add_user')
      fill_in 'invite_form_email', with: email
      click_on t('common.invite')
      last_invite = Invite.last
      expect(last_invite.user).to eq user
      expect(last_invite.email).to eq email
    end

    scenario 'user can not invite users if family is full' do
      allow(user.family).to receive(:full?).and_return(true)
      visit profile_path
      expect(page).to_not have_content t('common.invite')
    end
  end
end
