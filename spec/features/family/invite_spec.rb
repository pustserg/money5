require 'integration_helper'
feature 'invites to family' do
  let(:family_user) { create(:user) }
  let(:invite) { create(:invite, user: family_user) }
  scenario 'user tries to login with right invite' do
    visit root_path(invite: invite.code)
    auth_mock
    click_on t('common.login')
    sleep(0.5)
    user = User.last
    expect(user.family).to eq family_user.family
    expect(invite.reload.accepted_at.day).to eq Time.current.day
  end

  scenario 'user tries to login with fail invite' do
    visit root_path(invite: '12345')
    auth_mock
    click_on t('common.login')
    sleep(0.5)
    user = User.last
    expect(user.family).to_not eq family_user.family
  end
end
