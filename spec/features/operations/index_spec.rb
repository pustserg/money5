require 'integration_helper'

feature 'list of operations' do
  let(:family) { create(:family) }
  let(:family_user_1) { create(:user, family: family) }
  let(:family_user_2) { create(:user, family: family) }
  let(:another_user) { create(:user) }

  before do
    allow_any_instance_of(ApplicationController).to(
      receive(:current_user).and_return(family_user_1)
    )
  end

  context 'user sees operations by type' do
    let!(:income) { create(:income_operation, user: family_user_1) }
    let!(:outgo) { create(:outgo_operation, user: family_user_1) }
    scenario 'incomes' do
      visit root_path
      click_on t('operations.incomes')
      expect(page).to have_content income.title
    end
    scenario 'outgos' do
      visit root_path
      click_on t('operations.outgos')
      expect(page).to have_content outgo.title
    end
  end

  context 'user_family operations' do
    scenario 'user can see his operation' do
      operation = create(:income_operation, user: family_user_1)
      visit incomes_path
      expect(page).to have_content operation.title
    end

    scenario 'user can see same family user operation' do
      operation = create(:income_operation, user: family_user_2)
      visit incomes_path
      expect(page).to have_content operation.title
    end

    scenario 'user can not see another family user operation' do
      operation = create(:income_operation, user: another_user)
      visit incomes_path
      expect(page).to_not have_content operation.title
    end
  end

  context 'grouped by date operations' do
    let!(:this_month_operation) do
      create(:income_operation, user: family_user_1, date: Date.current)
    end
    let!(:prev_month_operation) do
      create(
        :income_operation, user: family_user_1, date: Date.current - 1.month
      )
    end

    scenario 'user sees this month operations only' do
      visit incomes_path
      expect(page).to have_content this_month_operation.title
      expect(page).to_not have_content prev_month_operation.title
    end

    scenario 'user sees operations for previous dates', js: true do
      visit incomes_path
      within '.opener' do
        click_on 'open-button'
      end
      within '.menu' do
        year = prev_month_operation.date.year
        month_name = Date::MONTHNAMES[prev_month_operation.date.month]
        page.find(:xpath, "//span[normalize-space()='#{year}']").click
        click_on t("common.dates.months.#{month_name}")
      end
      expect(page).to_not have_content this_month_operation.title
      expect(page).to have_content prev_month_operation.title
    end
  end
end
