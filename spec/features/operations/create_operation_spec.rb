require 'integration_helper'

feature 'add operations' do
  scenario 'guest cannot add operations' do
    visit incomes_path
    expect(page).to_not have_content('Add income')
    expect(page).to_not have_content('Add outgo')
    visit outgos_path
    expect(page).to_not have_content('Add income')
    expect(page).to_not have_content('Add outgo')
  end

  context 'Registered user' do
    let(:rand_amount) { Faker::Commerce.price }
    let(:rand_date) { Faker::Date.backward(14) }
    let(:rand_title) { Faker::Lorem.word }
    let(:rand_cat_list) { Faker::Lorem.words }
    let!(:user) { create(:user) }

    before do
      allow_any_instance_of(ApplicationController).to(
        receive(:current_user).and_return(user)
      )
    end

    scenario 'user adds income', js: true do
      visit incomes_path
      click_on t('operations.create_income')
      fill_in 'operation_form_title', with: rand_title
      fill_in 'operation_form_amount', with: rand_amount
      fill_in 'operation_form_date', with: rand_date
      # fill_in 'tags_input', with: rand_cat_list.join(', ')
      click_on t('common.save')
      operation = Operation.last
      expect(operation.is_a?(Operations::Income)).to be_truthy
      expect(operation.title).to eq rand_title
      expect(operation.amount.to_f).to eq rand_amount
      expect(operation.date).to eq rand_date
      # expect(
      # operation.categories.map(&:title)
      # ).to match_array rand_cat_list.uniq
    end

    scenario 'user adds outgo', js: true do
      visit outgos_path
      click_on t('operations.create_outgo')
      fill_in 'operation_form_title', with: rand_title
      fill_in 'operation_form_amount', with: rand_amount
      fill_in 'operation_form_date', with: rand_date
      # fill_in 'operation_form_categories_list', with: rand_cat_list.join(', ')
      click_on t('common.save')
      operation = Operation.last
      expect(operation.is_a?(Operations::Outgo)).to be_truthy
      expect(operation.title).to eq rand_title
      expect(operation.amount.to_f).to eq rand_amount
      expect(operation.date).to eq rand_date
      # expect(
      # operation.categories.map(&:title)
      # ).to match_array rand_cat_list.uniq
    end
  end

  context 'Free account limits' do
    let!(:user) { create(:user) }
    before do
      allow_any_instance_of(ApplicationController).to(
        receive(:current_user).and_return(user)
      )
    end

    context 'free account' do
      before { allow(user.family).to receive(:paid?).and_return(false) }
      scenario 'when limit is not reached' do
        visit incomes_path
        expect(page).to have_content t('operations.create_income')
      end
      scenario 'when limit is reached' do
        create_list(
          :operation,
          Family::FREE_MONTH_OPERATIONS_LIMIT + 1,
          user: user
        )
        visit incomes_path
        expect(page).to_not have_content t('operations.create_income')
      end
    end
    context 'paid account' do
      before { allow(user.family).to receive(:paid?).and_return(true) }
      scenario 'when limit is not reached' do
        visit incomes_path
        expect(page).to have_content t('operations.create_income')
      end
      scenario 'when limit is reached' do
        create_list(
          :operation,
          Family::FREE_MONTH_OPERATIONS_LIMIT + 1,
          user: user
        )
        visit incomes_path
        expect(page).to have_content t('operations.create_income')
      end
    end
  end
end
