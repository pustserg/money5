require 'integration_helper'

feature 'Sign in user' do
  scenario 'After creating user, we must create family' do
    expect(User.count).to eq 0
    visit root_path
    auth_mock
    click_on t('common.login')
    sleep(0.5)
    user = User.last
    expect(user.family_title).to eq user.name
  end
end
