require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
Dotenv::Railtie.load

module Money5
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over
    # those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.generators do |g|
      g.test_framework   :rspec, :fixture => false
      g.view_specs       false
      g.helper_specs     false
      g.controller_specs false
    end

    config.i18n.default_locale = :ru
    config.active_job.queue_adapter = :sidekiq
  end
end
