Rails.application.routes.draw do
  # For details on the DSL available within this file, see
  # http://guides.rubyonrails.org/routing.html

  resources :operations, except: :index
  get '/income/(:year)/(:month)',
      to: 'operations#index',
      as: :incomes,
      type: 'income'
  get '/outgo/(:year)/(:month)',
      to: 'operations#index',
      as: :outgos,
      type: 'outgo'

  get 'auth/:provider/callback', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'

  get '/profile', to: 'users#show', as: :profile
  resource :user, only: :update
  resources :invites, only: %i(create destroy)
end
