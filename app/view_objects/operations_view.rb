class OperationsView
  def initialize(user, type, period = {})
    @user = user
    @type = type
    @month = period[:month] || Date.current.month
    @year = period[:year] || Date.current.year
  end

  def index
    return Operation.none unless @user
    user_operations
      .includes(:user)
      .where('operations.date' => all_month)
      .order(date: :desc, created_at: :desc)
  end

  def total
    return unless @user
    user_operations.where(date: all_month).sum(:amount)
  end

  def dates_hash_for_side_menu
    return {} unless @user
    {}.tap do |h|
      years.each do |year|
        h[year] = years_months
                  .select { |date| date[0] == year }
                  .map { |date| date[1] }
                  .sort.reverse!
      end
    end
  end

  private

  def user_operations
    @user_operations ||= if @type == 'income'
                           @user.family_operations.incomes
                         elsif @type == 'outgo'
                           @user.family_operations.outgos
                         else
                           @user.family_operations
                         end
  end

  def all_month
    DateTime.new(@year.to_i, @month.to_i, 1).all_month
  end

  def years_months
    @arr ||= user_operations
             .group("DATE_TRUNC('month', date)").count.keys
             .map { |date| [date.year, date.month] }
  end

  def years
    @years ||= years_months.map { |d| d[0] }.uniq.sort.reverse!
  end
end
