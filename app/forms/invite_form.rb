class InviteForm
  include Virtus.model
  include ActiveModel::Validations
  include ActiveModel::Model

  attribute :user, User
  attribute :email, String

  validates :email, presence: true

  def save
    persist! if valid?
  end

  def persist!
    invite = user.invites.create!(email: email, code: build_code)
    InviteMailerScheduleWorker.perform_async(invite.id)
  end

  private

  def build_code
    string = "#{user.email}#{user.family_title}#{Time.current.to_i}"
    Digest::MD5.hexdigest(string)
  end
end
