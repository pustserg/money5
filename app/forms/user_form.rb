class UserForm
  include Virtus.model
  include ActiveModel::Validations
  include ActiveModel::Model

  attribute :family_title
  attribute :family_paid, Boolean

  validates :family_title, presence: true
  delegate :family, to: :user
  attr_reader :user
  def initialize(user, params = {})
    super(params)
    @user = user
    # TODO: maybe it is redunant. We always creates family for user.
    self.family_title ||= @user.family_title
  end

  def save
    persist! if valid?
  end

  def persist!
    ActiveRecord::Base.transaction do
      family.update!(family_params)
    end
  end

  private

  def family_params
    { title: family_title }
  end
end
