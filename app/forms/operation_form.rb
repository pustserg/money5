class OperationForm
  include Virtus.model
  include ActiveModel::Validations
  include ActiveModel::Model

  attribute :amount, Float, default: 0
  attribute :date, Date, default: Date.current
  attribute :title, String
  attribute :type, String
  attribute :categories_list, String

  validates :amount, :date, presence: true
  validates :title, length: { maximum: 50 }
  validate :free_limits

  def initialize(params = {}, user = nil)
    super(params.except(:type))
    @user = user
    @type = params[:type]
  end

  def save
    persist! if valid?
  end

  def persist!
    ActiveRecord::Base.transaction do
      operation = operation_class.create!(
        user: @user, title: title,
        amount: amount, date: date
      )
      create_categories(operation) if categories_list
      CalculateUserBalanceJob.perform_later(@user.id)
    end
  end

  private

  def operation_class
    "Operations::#{@type.capitalize}".constantize
  end

  def create_categories(operation)
    categories_list.split(', ').map(&:presence).compact.each do |cat_title|
      operation.categories.find_or_create_by!(title: cat_title, user: @user)
    end
  end

  def free_limits
    return if @user.paid?
    return if @user.family_operations.this_month.count <
              Family::FREE_MONTH_OPERATIONS_LIMIT
    errors.add :base, 'Free operations limit exceed'
  end
end
