class InviteMailerScheduleWorker
  # TODO: redo with ActiveJob
  include Sidekiq::Worker

  def perform(invite_id)
    InvitesMailer.invite_user(invite_id).deliver_now
    invite = Invite.find(invite_id)
    invite.update!(sent_at: Time.current)
  end
end
