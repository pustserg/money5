class Category < ApplicationRecord
  validates :title, presence: true
  validates :title, length: { maximum: 50 }
  validates :title, uniqueness: { scope: [:user_id, :type] }
  has_many :operation_categories, dependent: :destroy
  belongs_to :user

  delegate :family, to: :user
end
