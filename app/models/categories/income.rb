class Categories::Income < Category
  has_many :operations,
           class_name: 'Operations::Income',
           through: :operation_categories
end
