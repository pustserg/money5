class Categories::Outgo < Category
  has_many :operations,
           class_name: 'Operations::Outgo',
           through: :operation_categories
end
