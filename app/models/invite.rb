class Invite < ApplicationRecord
  belongs_to :user
  validates :user_id, :email, :code, presence: true
  delegate :name, to: :user, prefix: :user
  delegate :family, to: :user

  scope :accepted, -> { where.not(accepted_at: nil) }
  scope :waiting, -> { where(accepted_at: nil) }

  def accepted?
    accepted_at.present?
  end
end
