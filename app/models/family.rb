class Family < ApplicationRecord
  # TODO: need to validate while user is creating
  MAX_USERS_COUNT = 3
  FREE_MONTH_OPERATIONS_LIMIT = 50
  has_many :users
  has_many :categories, through: :users
  has_many :operations, through: :users
  has_many :invites, through: :users

  def full?
    (users.count + invites.waiting.count) >= MAX_USERS_COUNT
  end

  def current_balance_amount
    users.sum(&:current_balance_amount)
  end
end
