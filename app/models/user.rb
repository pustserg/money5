class User < ApplicationRecord
  validates :name, presence: true

  has_many :operations
  has_many :identities, dependent: :destroy
  has_many :categories, dependent: :destroy
  has_many :invites, dependent: :destroy
  has_many :balances, dependent: :destroy
  belongs_to :family, required: false

  delegate :title, :operations, to: :family, prefix: :family
  delegate :paid?, to: :family
  delegate :amount, to: :current_month_balance, prefix: :current_month_balance

  def current_month_balance
    current_year = Date.current.year
    current_month = Date.current.month
    balances.find_or_create_by(year: current_year, month: current_month)
  end

  def month_balance(year, month)
    balances.where(year: year, month: month).sum(:amount)
  end

  def current_balance_amount
    balances.sum(:amount)
  end
end
