class Balance < ApplicationRecord
  validates :amount, :year, :month, presence: true
  belongs_to :user
end
