class Operation < ApplicationRecord
  validates :amount, :date, presence: true
  validates :title, length: { maximum: 50 }
  belongs_to :user
  has_many :operation_categories, dependent: :destroy

  delegate :name, to: :user, prefix: true
  delegate :family, to: :user

  scope :incomes, -> { where(type: 'Operations::Income') }
  scope :outgos, -> { where(type: 'Operations::Outgo') }
  scope :by_period, ->(start, finish) { where(date: start..finish) }

  class << self
    def this_month
      start_date = Time.current.beginning_of_month
      end_date = Time.current.end_of_month
      by_period(start_date, end_date)
    end
  end
end
