class Operations::Outgo < Operation
  has_many :categories,
           class_name: 'Categories::Outgo',
           through: :operation_categories
end
