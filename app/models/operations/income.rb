class Operations::Income < Operation
  has_many :categories,
           class_name: 'Categories::Income',
           through: :operation_categories
end
