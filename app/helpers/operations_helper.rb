module OperationsHelper
  def active?(controller_name, type = nil)
    controller.controller_name == controller_name && params[:type] == type
  end

  def side_menu_link_class(params, year, month)
    if year == params[:year].to_i && month == params[:month].to_i
      return 'active'
    end
    return 'active' if current_year_and_month?(params, year, month)
  end

  def current_year_and_month?(params, year, month)
    !params[:year] && !params[:month] &&
      year == Date.current.year &&
      month == Date.current.month
  end
end
