module FamiliesHelper
  def used_operations_ratio(family)
    current_operations_count = family.operations.this_month.count
    "#{current_operations_count}/#{Family::FREE_MONTH_OPERATIONS_LIMIT}"
  end
end
