class SessionsController < ApplicationController
  def create
    user = UserAuthentificator
           .new(request.env['omniauth.auth'], session[:invite])
           .call
    if user
      login_user(user)
      flash[:notice] = t('common.login_succes')
    else
      flash[:error] = t('common.login_failed')
    end
    redirect_to root_path
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_path, notice: t('common.logout_success')
  end
end
