class InvitesController < ApplicationController
  def create
    @form = InviteForm.new(invite_params)
    if @form.save
      flash[:success] = t('invites.flash.invite_sent')
    else
      flash[:error] = @form.errors.full_messages.join('<br>')
    end
    redirect_back(fallback_location: profile_path)
  end

  def destroy
    invite = Invite.find(params[:id])
    if invite.destroy
      flash[:success] = t('invites.flash.delete_success')
    else
      flash[:error] = invite.errors.full_messages.join('<br>')
    end
    redirect_back(fallback_location: profile_path)
  end

  private

  def invite_params
    params.require(:invite_form).permit(:email).merge(user: current_user)
  end
end
