class UsersController < ApplicationController
  def show
    @user_form = UserForm.new(current_user)
    @invite_form = InviteForm.new(user: current_user)
    @family = Family.includes(:users, :invites).find(current_user.family_id)
  end

  def update
    @user_form = UserForm.new(current_user, form_params)
    if @user_form.save
      flash[:success] = t('users.flash.updated')
    else
      flash[:error] = "#{t('users.flash.not_updated')}. "\
                      "#{@user_form.errors.full_messages.join('<br>')}"
    end
    redirect_back(fallback_location: profile_path)
  end

  private

  def form_params
    params.require(:user_form).permit(:family_title)
  end
end
