class OperationsController < ApplicationController
  def index
    @operations = OperationsView
                  .new(
                    current_user,
                    params[:type],
                    month: params[:month],
                    year: params[:year]
                  )
    @new_operation = OperationForm.new(type: params[:type])
  end

  def edit
  end

  def create
    @form = OperationForm.new(operation_params, current_user)
    if @form.save
      flash[:success] = t('operations.flash.created')
    else
      flash[:error] = t('operations.flash.not_created')
    end
    redirect_back(fallback_location: operations_path)
  end

  def update
  end

  def destroy
  end

  private

  def operation_params
    params
      .require(:operation_form)
      .permit(:type, :amount, :date, :title, :categories_list)
  end
end
