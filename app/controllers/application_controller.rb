class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :store_invite

  private

  def current_user
    return unless session[:user_id]
    @current_user ||= User.includes(:family).find(session[:user_id])
  end

  def login_user(user)
    return unless user
    session[:user_id] = user.id
  end

  def store_invite
    session[:invite] = params[:invite] if params[:invite]
  end

  helper_method :current_user
end
