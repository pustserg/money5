class CalculateUserBalanceJob < ApplicationJob
  queue_as :default

  def perform(user_id)
    BalanceCalculator.new(user_id).call
  end
end
