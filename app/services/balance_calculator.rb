class BalanceCalculator
  def initialize(user_id)
    @user = User.find(user_id)
  end

  def call(mode = :current_month, year = nil, month = nil)
    if mode == :current_month
      calculate_current_user_balance
    elsif mode == :all
      user_year_months.each do |c_year, c_month|
        calculate_month_balance(c_year, c_month)
      end
    else
      calculate_month_balance(year, month)
    end
  end

  private

  def calculate_current_user_balance
    calculate_month_balance(Date.current.year, Date.current.month)
  end

  def calculate_month_balance(year, month)
    balance = @user
              .balances
              .find_or_initialize_by(year: year, month: month)
    balance.amount = incomes_sum(year, month) - outgos_sum(year, month)
    balance.save
  end

  def incomes_sum(year, month)
    @user
      .operations
      .incomes
      .by_period(start(year, month), finish(year, month))
      .sum(:amount)
  end

  def outgos_sum(year, month)
    @user
      .operations
      .outgos
      .by_period(start(year, month), finish(year, month))
      .sum(:amount)
  end

  def start(year, month)
    DateTime.new(year, month, 1).beginning_of_month
  end

  def finish(year, month)
    DateTime.new(year, month, 1).end_of_month
  end

  def user_year_months
    @user.operations.pluck('distinct date').map { |d| [d.year, d.month] }.uniq
  end
end
