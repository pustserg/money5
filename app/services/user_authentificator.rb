class UserAuthentificator
  def initialize(params, invite_code = nil)
    @provider = params['provider']
    @uid = params['uid']
    @info = params['info']
    @invite_code = invite_code
  end

  def call
    found_identity = Identity.includes(:user).find_by(uid: @uid)
    return found_identity.user if found_identity
    create_user_with_identity
  end

  private

  def create_user_with_identity
    ActiveRecord::Base.transaction do
      # TODO: extract family to function. Invite can have lifetime
      family = find_family_by_invite_code if @invite_code
      family ||= Family.create(title: @info['name'])
      user = User.create!(
        name: @info['name'], email: @info['email'], family: family
      )
      user.identities.create!(provider: @provider, uid: @uid)
      mark_invite_as_accepted if @invite_code
      user
    end
  end

  def find_family_by_invite_code
    invite = Invite.find_by(code: @invite_code)
    return unless invite
    invite.family
  end

  def mark_invite_as_accepted
    invite = Invite.find_by(code: @invite_code)
    return unless invite
    invite.update!(accepted_at: Time.current)
  end
end
