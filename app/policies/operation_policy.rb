class OperationPolicy
  attr_reader :user

  def initialize(user)
    @user = user
  end

  def create?
    return false unless user
    return true if user.paid?
    user.family_operations.this_month.count <
      Family::FREE_MONTH_OPERATIONS_LIMIT
  end
end
