# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#= require 'calendar_selector'

$ ->
  currentLocale = $('body').data('locale') || 'en'

  $(document).on 'click', 'a.new-operation', (e) ->
    e.preventDefault()
    $('#form').modal('toggle')
    $('#form #tags').html('')

    $('form#new_operation_form .datepicker').datepicker(
      format: 'yyyy-mm-dd'
      autoclose: true
      todayHighlight: true
      todayBtn: true
      language: currentLocale
    )

    $('form#new_operation_form #tags_input').keyup (e)->
      return true unless e.keyCode is 188
      category = e.target.value.replace(',', '').trim()
      $(e.target).val('')
      return if category is ''
      addCategoryToHiddenField(category)

    $('form#new_operation_form #tags').click (e) ->
      $target = $(e.target)
      category = $target.text()
      $hidden_input = $('#new_operation_form #operation_form_categories_list')
      current_value = $hidden_input.val()
      new_value = current_value.replace(", #{category}", '')
      $hidden_input.val("#{new_value}")
      $target.remove()

    $('form#new_operation_form').submit (e) ->
      category = $(e.target).find('#tags_input').val().trim()
      addCategoryToHiddenField(category) unless category is ''

addCategoryToHiddenField = (category) ->
  hidden_input = $('#operation_form_categories_list')
  current_categories_list = hidden_input.val()
  hidden_input.val("#{current_categories_list}, #{category}")
  span = "<span class='label label-info'>#{category}</span>"
  $('#tags').append(span)

