$ ->
  $(document).on 'click', '.toggle-user-form', (e) ->
    toggleForm('user-form', e)
  $(document).on 'click', '.toggle-invite-form', (e) ->
    toggleForm('invite-form', e)

  toggleForm = (formClass, event) ->
    event.preventDefault()
    $(".#{formClass} .value").toggle()
    $(".#{formClass} .form-input").toggle()
    $(event.target).closest('.actions').find('button').each (i, el) ->
      $(el).toggle()
