$(document).on 'click', '.side-menu.opener button', (e) ->
  openCalendar()

$(document).on 'click', '.side-menu #close-button', (e) ->
  closeCalendar()

$(document).on 'click', '#my-accordion .panel-heading span', (e) ->
  toShow = $(e.target).data('toggle')
  $('#my-accordion .panel-body:visible').addClass('hidden')
  $("#my-accordion .panel-body[data-toggled-by=#{toShow}]")
    .removeClass('hidden')

openCalendar = ->
  localStorage.setItem('calendar_state', 'opened') if localStorage
  $('.operations-side-menu.opener').addClass('hidden')
  $('.operations-side-menu.closer').removeClass('hidden')
  $('.operations-side-menu.menu').removeClass('hidden')

closeCalendar = ->
  localStorage.setItem('calendar_state', 'closed') if localStorage
  $('.operations-side-menu.menu').addClass('hidden')
  $('.operations-side-menu.closer').addClass('hidden')
  $('.operations-side-menu.opener').removeClass('hidden')

$(document).on 'turbolinks:load', ->
  calendar_state = localStorage.getItem('calendar_state') if localStorage
  return unless calendar_state
  if calendar_state == 'opened'
    openCalendar()
  else if calendar_state == 'closed'
    closeCalendar()
