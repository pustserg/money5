class InvitesMailer < ApplicationMailer
  def invite_user(invite_id)
    @invite = Invite.includes(:user).find(invite_id)
    mail(to: @invite.email, subject: 'Invite')
  end
end
