class ApplicationMailer < ActionMailer::Base
  default from: 'info@money.os'
  layout 'mailer'
end
